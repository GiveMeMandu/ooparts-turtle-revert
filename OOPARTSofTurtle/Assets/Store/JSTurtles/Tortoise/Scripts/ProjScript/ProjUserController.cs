﻿using UnityEngine;
using System.Collections;

public class ProjUserController : MonoBehaviour {
	ProjCharacter ProjCharacter;
    public float step = 0.01f, leng = 0.7f;
    float r, l;

	void Start () {
		ProjCharacter = GetComponent < ProjCharacter> ();
	}
	
	void Update () {

        if (Input.GetKeyDown(KeyCode.I)) { ProjCharacter.FRD(); }
        if (Input.GetKeyUp(KeyCode.I)) { ProjCharacter.FRU();  r = Mathf.Max(r, Time.time + leng); }
        if (Input.GetKeyDown(KeyCode.J)) { ProjCharacter.BRD(); r = Mathf.Max(r, Time.time + leng); }
        if (Input.GetKeyUp(KeyCode.J)) { ProjCharacter.BRU(); }
        if (Input.GetKeyDown(KeyCode.E)) { ProjCharacter.FLD(); l = Mathf.Max(l, Time.time + leng); }
        if (Input.GetKeyUp(KeyCode.E)) { ProjCharacter.FLU(); }
        if (Input.GetKeyDown(KeyCode.F)) { ProjCharacter.BLD(); }
        if (Input.GetKeyUp(KeyCode.F)) { ProjCharacter.BLU(); l = Mathf.Max(l, Time.time + leng); }

        bool ra, la;
        ra = r >= Time.time;
        la = l >= Time.time;
        if( la && ra )
        {
            transform.Translate(Vector3.forward * step * Time.deltaTime);
        }
        else if ( la && !ra )
        {
            
        }
        else if( !la && ra )
        {

        }
        /*
		if (Input.GetButtonDown ("Fire1")) {
			ProjCharacter.Attack();
		}
		
		if (Input.GetKeyDown (KeyCode.H)) {
			ProjCharacter.Hit();
		}

		if (Input.GetKeyDown (KeyCode.E)) {
			ProjCharacter.Eat();
		}

		if (Input.GetKeyDown (KeyCode.Z)) {
			ProjCharacter.SitDown();
		}
		
		if (Input.GetKeyUp (KeyCode.Z)) {
			ProjCharacter.StandUp();
		}

		if (Input.GetKeyDown (KeyCode.K)) {
			ProjCharacter.Death1();
		}

		if (Input.GetKeyDown (KeyCode.L)) {
			ProjCharacter.Death2();
		}

		if (Input.GetKeyDown (KeyCode.M)) {
			ProjCharacter.Death3();
		}

		if (Input.GetKeyDown (KeyCode.R)) {
			ProjCharacter.Rebirth();
		}
        */
    }
	/*
	private void FixedUpdate()
	{
		float h = Input.GetAxis ("Horizontal");
		float v = Input.GetAxis ("Vertical");
		ProjCharacter.Move (v,h);
	}
    */
}
