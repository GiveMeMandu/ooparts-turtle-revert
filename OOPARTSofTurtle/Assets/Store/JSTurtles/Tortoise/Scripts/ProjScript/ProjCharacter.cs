﻿using UnityEngine;
using System.Collections;

public class ProjCharacter : MonoBehaviour {
	Animator ProjAnimator;
	
	void Start () {
		ProjAnimator = GetComponent<Animator> ();
	}

    public void FRD() { ProjAnimator.SetBool("FR", true); }
    public void FRU() { ProjAnimator.SetBool("FR", false); }
    public void BRD() { ProjAnimator.SetBool("BR", true); }
    public void BRU() { ProjAnimator.SetBool("BR", false); }
    public void FLD() { ProjAnimator.SetBool("FL", true); }
    public void FLU() { ProjAnimator.SetBool("FL", false); }
    public void BLD() { ProjAnimator.SetBool("BL", true); }
    public void BLU() { ProjAnimator.SetBool("BL", false); }
    
    /*
    public void Attack(){
		ProjAnimator.SetTrigger("Attack");
	}

	public void Hit(){
		ProjAnimator.SetTrigger("Hit");
	}

	public void Eat(){
		ProjAnimator.SetTrigger("Eat");
	}

	public void SitDown(){
		ProjAnimator.SetBool("SitDown",true);
	}
	
	public void StandUp(){
		ProjAnimator.SetBool("SitDown",false);
	}

	public void Death1(){
		ProjAnimator.SetTrigger("Death1");
	}

	public void Death2(){
		ProjAnimator.SetTrigger("Death2");
	}

	public void Death3(){
		ProjAnimator.SetTrigger("Death3");
	}

	public void Rebirth(){
		ProjAnimator.SetTrigger("Rebirth");
	}

	public void Move(float v,float h){
		ProjAnimator.SetFloat ("Forward", v);
		ProjAnimator.SetFloat ("Turn", h);
	}
    */
}
