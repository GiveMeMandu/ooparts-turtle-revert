﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjTurtleMv : MonoBehaviour
{
    public KeyCode assKey;
    public float dur;
    public AnimationCurve AC;
    public bool whenKeyUp;
    public float fwDeg, rotDeg;
    bool onRunning;
    public float progress;
    public float EV;

    delegate bool KeyFunc(KeyCode K);
    KeyFunc eventFunc;

    void Start()
    {
        if(whenKeyUp)
        {
            eventFunc = new KeyFunc(Input.GetKeyUp);
        }
        else
        {
            eventFunc = new KeyFunc(Input.GetKeyDown);
        }
    }

    
    void Update()
    {
        if (onRunning)
        {
            progress += Time.deltaTime;
            EV = AC.Evaluate((progress - Time.deltaTime / 2) / dur) - 1;
            transform.Rotate(Vector3.up * rotDeg * EV * Time.deltaTime);
            transform.Translate(Vector3.forward * fwDeg * EV * Time.deltaTime);
        }
        if( progress > dur )
        {
            progress = 0;
            onRunning = false;
        }
        if(eventFunc(assKey))
        {
            onRunning = true;
        }
        //transform.Translate(Vector3.forward * 5f);
    }
}
