%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: FR
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: AfricanSpurredTortoise
    m_Weight: 0
  - m_Path: Root
    m_Weight: 0
  - m_Path: Root/Pelvis
    m_Weight: 0
  - m_Path: Root/Pelvis/Spine
    m_Weight: 0
  - m_Path: Root/Pelvis/Spine/Clavicle_L
    m_Weight: 0
  - m_Path: Root/Pelvis/Spine/Clavicle_L/UpperArm_L
    m_Weight: 0
  - m_Path: Root/Pelvis/Spine/Clavicle_L/UpperArm_L/Forearm_L
    m_Weight: 0
  - m_Path: Root/Pelvis/Spine/Clavicle_L/UpperArm_L/Forearm_L/Hand_L
    m_Weight: 0
  - m_Path: Root/Pelvis/Spine/Clavicle_R
    m_Weight: 1
  - m_Path: Root/Pelvis/Spine/Clavicle_R/UpperArm_R
    m_Weight: 1
  - m_Path: Root/Pelvis/Spine/Clavicle_R/UpperArm_R/Forearm_R
    m_Weight: 1
  - m_Path: Root/Pelvis/Spine/Clavicle_R/UpperArm_R/Forearm_R/Hand_R
    m_Weight: 1
  - m_Path: Root/Pelvis/Spine/Neck.1
    m_Weight: 0
  - m_Path: Root/Pelvis/Spine/Neck.1/Neck.2
    m_Weight: 0
  - m_Path: Root/Pelvis/Spine/Neck.1/Neck.2/Neck.3
    m_Weight: 0
  - m_Path: Root/Pelvis/Spine/Neck.1/Neck.2/Neck.3/Head
    m_Weight: 0
  - m_Path: Root/Pelvis/Spine/Neck.1/Neck.2/Neck.3/Head/Eye_L
    m_Weight: 0
  - m_Path: Root/Pelvis/Spine/Neck.1/Neck.2/Neck.3/Head/Eye_R
    m_Weight: 0
  - m_Path: Root/Pelvis/Spine/Neck.1/Neck.2/Neck.3/Head/FirstEyelid_L
    m_Weight: 0
  - m_Path: Root/Pelvis/Spine/Neck.1/Neck.2/Neck.3/Head/FirstEyelid_R
    m_Weight: 0
  - m_Path: Root/Pelvis/Spine/Neck.1/Neck.2/Neck.3/Head/Jaw
    m_Weight: 0
  - m_Path: Root/Pelvis/Spine/Neck.1/Neck.2/Neck.3/Head/Jaw/Tongue.1
    m_Weight: 0
  - m_Path: Root/Pelvis/Spine/Neck.1/Neck.2/Neck.3/Head/Jaw/Tongue.1/Tongue.3
    m_Weight: 0
  - m_Path: Root/Pelvis/Spine/Neck.1/Neck.2/Neck.3/Head/Jaw.1
    m_Weight: 0
  - m_Path: Root/Pelvis/Spine/Neck.1/Neck.2/Neck.3/Head/SecondEyelid_L
    m_Weight: 0
  - m_Path: Root/Pelvis/Spine/Neck.1/Neck.2/Neck.3/Head/SecondEyelid_R
    m_Weight: 0
  - m_Path: Root/Pelvis/Spine/Neck.1/Neck.2/Neck.3/Head/ThirdEyelid_L
    m_Weight: 0
  - m_Path: Root/Pelvis/Spine/Neck.1/Neck.2/Neck.3/Head/ThirdEyelid_R
    m_Weight: 0
  - m_Path: Root/Pelvis/Tail.1
    m_Weight: 0
  - m_Path: Root/Pelvis/Tail.1/Tail.2
    m_Weight: 0
  - m_Path: Root/Pelvis/Thigh_L
    m_Weight: 0
  - m_Path: Root/Pelvis/Thigh_L/Calf_L
    m_Weight: 0
  - m_Path: Root/Pelvis/Thigh_L/Calf_L/Foot_L
    m_Weight: 0
  - m_Path: Root/Pelvis/Thigh_R
    m_Weight: 0
  - m_Path: Root/Pelvis/Thigh_R/Calf_R
    m_Weight: 0
  - m_Path: Root/Pelvis/Thigh_R/Calf_R/Foot_R
    m_Weight: 0
