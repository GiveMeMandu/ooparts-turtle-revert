﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuButtonEventHandler : MonoBehaviour
{
    public GameObject MenuListContentObject;
    public GameObject AchievementViewPrefab;

    private MenuListState nowState = MenuListState.EMPTY;

    public void OnClickMenuOptionButton()
    {
        UpdateMenuListContentObject(MenuListState.OPTION);
    }

    public void OnClickMenuAchievementButton()
    {
        UpdateMenuListContentObject(MenuListState.ACHIEVEMENT);
    }

    public void OnClickMenuSaveAndExitButton()
    {

    }

    public void OnClickUnlockAllAchievementButton()
    {
        AchievementManager.Instance.UnlockAllAchievements();
    }

    private void UpdateMenuListContentObject(MenuListState nextState)
    {
        nextState = ToggleMenuListState(nextState);

        switch (nextState)
        {
            case MenuListState.EMPTY:
                MakeMenuListEmpty();
                break;
            case MenuListState.OPTION:
                MakeMenuListOption();
                break;
            case MenuListState.ACHIEVEMENT:
                MakeMenuListAchievement();
                break;
        }

        Debug.Log(nextState);
    }

    private MenuListState ToggleMenuListState(MenuListState nextState)
    {
        MenuListState result = nextState;

        if (nowState == result)
        {
            result = MenuListState.EMPTY;
        }

        nowState = result;
        return result;
    }

    private void MakeMenuListEmpty()
    {
        foreach (Transform child in MenuListContentObject.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

    private void MakeMenuListOption()
    {

    }

    private void MakeMenuListAchievement()
    {
        AchievementManager.Instance.InitAchievementViews();
        var am = AchievementManager.Instance.Achievements;
        foreach (var achievement in am)
        {
            var viewObject = GameObject.Instantiate(AchievementViewPrefab, new Vector3(0, 0, 0), Quaternion.identity);
            viewObject.transform.SetParent(MenuListContentObject.transform);
            viewObject.transform.localScale = Vector3.one;

            var viewScript = viewObject.GetComponent<AchievementView>();
            viewScript.InitAchievementView(achievement);
        }
    }

    private enum MenuListState
    {
        EMPTY,
        OPTION,
        ACHIEVEMENT,
    }
}
