﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementView : MonoBehaviour
{
    public Text titleText;
    public Text descriptionText;
    public Text dateText;
    public Image image;
    public AchievementManager.Achievement achievement;

    public void InitAchievementView(AchievementManager.Achievement achievement)
    {
        this.achievement = achievement;
        AchievementManager.Instance.RegisterAchievementViews(this);
        UpdateAchievementView();
    }

    public void UpdateAchievementView()
    {
        titleText.text = achievement.Title;
        descriptionText.text = achievement.Description;
        dateText.text = achievement.Date.ToString();
    }
}
