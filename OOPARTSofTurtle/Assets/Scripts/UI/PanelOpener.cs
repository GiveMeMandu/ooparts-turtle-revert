﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelOpener : MonoBehaviour
{
    public Animator MenuPanel;

    public void OpenMenuPanel()
    {
        MenuPanel.SetBool("open", !MenuPanel.GetBool("open"));
    }
}
