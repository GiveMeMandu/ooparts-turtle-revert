﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class AchievementManager
{
    private Achievement[] m_Achievements = new Achievement[] {
        new Achievement(AchievementEnum.BELIEVEOFMOTHERS, "엄마들의 믿음", "우리 튼튼이는 재능은 있는데 연습을 안해서 그래요.\n튼튼이는 어릴 적부터 수영을 할 줄 알았어요.\n이만하면 달리기 선수가 되는 건 문제가 없을텐데...", DateTime.Now, true),
        new Achievement(AchievementEnum.BROKEN_DREAM, "부서져버린 꿈", "이게 자연의 순리니까 너무 섭섭하게 생각하지는 말고.\n나는 간다. 바위~", DateTime.Now, true),
    };

    private List<AchievementView> m_AchievementViewObserver;

    public void InitAchievementViews()
    {
        m_AchievementViewObserver = new List<AchievementView>();
    }

    public void RegisterAchievementViews(AchievementView achievementView)
    {
        m_AchievementViewObserver.Add(achievementView);
    }

    public void UpdateAchievementViews()
    {
        foreach (var ahcievementView in m_AchievementViewObserver)
        {
            ahcievementView.UpdateAchievementView();
        }
    }

    public Achievement[] Achievements
    {
        get
        {
            return m_Achievements;
        }
    }

    private static readonly Lazy<AchievementManager> lazy
        = new Lazy<AchievementManager>(() => new AchievementManager());

    public static AchievementManager Instance
        => lazy.Value;

    private AchievementManager()
    {
        // Do nothing.
    }

    public void UnlockAchievement(AchievementEnum achievementID)
    {
        m_Achievements[(int)achievementID].IsAchieved = true;
        m_Achievements[(int)achievementID].Date = DateTime.Now;
        UpdateAchievementViews();
    }

    public void UnlockAllAchievements()
    {
        foreach (AchievementEnum suit in Enum.GetValues(typeof(AchievementEnum)))
        {
            UnlockAchievement(suit);
        }
    }

    public enum AchievementEnum : int
    {
        BELIEVEOFMOTHERS = 0,
        BROKEN_DREAM,
    }

    public class Achievement
    {
        public AchievementEnum AchievementID;
        private string m_title;
        private string m_description;
        private bool m_isHided;

        public string Title
        {
            get
            {
                return IsHided ? Regex.Replace(m_title, @"[^ \n]", "X") : m_title;
            }
            set
            {
                m_title = value;
            }
        }
        public string Description
        {
            get
            {
                return IsHided ? Regex.Replace(m_description, @"[^ \n]", "X") : m_description;
            }
            set
            {
                m_description = value;
            }
        }
        public bool IsAchieved;
        public bool IsHided
        {
            get
            {
                return !IsAchieved && m_isHided;
            }
            set
            {
                m_isHided = value;
            }
        }
        public DateTime Date;

        public Achievement(AchievementEnum achievmentID, string title, string description, DateTime date, bool isHided = false)
        {
            AchievementID = achievmentID;
            Title = title;
            Description = description;
            IsAchieved = false;
            Date = date;
            IsHided = isHided;
        }
    }
}